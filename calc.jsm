<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>My Web Page</title>

<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>






</head>
<body><div id="main">


<form name="operationform">
	<label for="firstnum">First Number:</label>
	<input type="text" name="firstnum" id="firstnum">
	<br><br>
	<label for="secondnum">Second Number:</label>
	<input type="text" name="secondnum" id="secondnum">
	<br><br>
	<input type="radio" name="operation" id="plus" value="plus">+
	<input type="radio" name="operation" id="minus" value="minus">-
	<input type="radio" name="operation" id="times" value="plus">*
	<input type="radio" name="operation" id="divide" value="minus">/
	<br><br>
	<label for="solution">Solution:</label>
	<input type="text" name="solution" id="solution">
</form>




</div>



<script type="text/javascript">

main();

var num1;
var num2;
var num3;
var operation = "unknown";


function main(){

	document.getElementById("plus").addEventListener("click",compute,false);
	document.getElementById("minus").addEventListener("click",compute,false);
	document.getElementById("times").addEventListener("click",compute,false);
	document.getElementById("divide").addEventListener("click",compute,false);
	

	document.getElementById("firstnum").addEventListener("keyup",compute,false);
	document.getElementById("secondnum").addEventListener("keyup",compute,false);

}

function compute(){

	num1 = document.getElementById("firstnum").value;
	num2 = document.getElementById("secondnum").value;

	if (isNaN(num1) || isNaN(num2)){
		document.getElementById("solution").textContent = "invalid number entered";
		return;
	}
	
	operation = (document.getElementById("plus").checked) ? "add" : operation;	
	operation = (document.getElementById("minus").checked) ? "subtract" : operation;	
	operation = (document.getElementById("times").checked) ? "multiply" : operation;	
	operation = (document.getElementById("divide").checked) ? "divide" : operation;	

	switch(operation){
		case "add":
			num3 = (Number(num1) + Number(num2));
			break;
		case "subtract":
			num3 = (num1 - num2);
			break;
		case "multiply":
			num3 = (num1*num2);
			break;
		case "divide":
			if (num2 == 0){
				num3 = "cant divide by 0";
			} else {
				num3 = (num1 / num2);
			}
			break;
		default :
			num3 = "no radio button checked";
	}
	document.getElementById("solution").value = num3;
}



</script>



</body>
</html>

